addSbtPlugin("com.github.sbt" % "sbt-git" % "2.1.0")
addSbtPlugin("com.eed3si9n" % "sbt-assembly" % "2.3.0")
addSbtPlugin("org.scoverage" % "sbt-scoverage" % "2.2.2")
addSbtPlugin("org.scalastyle" %% "scalastyle-sbt-plugin" % "1.0.0")
addSbtPlugin("nl.zolotko.sbt" % "sbt-gitlab" % "0.1.4")

ThisBuild / libraryDependencySchemes += "org.scala-lang.modules" % "scala-xml_2.12" % VersionScheme.Always
